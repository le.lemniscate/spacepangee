package io.spacepangee.backend.utils

import io.vavr.control.Either

fun <L, R> List<Either<L, R>>.toEitherOfList(): Either<L, List<R>> =
    try {
        val error = this.first { it.isLeft }
        Either.left(error.left)
    } catch (e: NoSuchElementException) {
        Either.right(this.map { it.get()})
    }

