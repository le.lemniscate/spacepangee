package io.spacepangee.backend.domain.repositories

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.Nation
import io.vavr.control.Either

interface WriteNations {
    fun save(nation: Nation): Either<SpacePangeeError, Nation>
}