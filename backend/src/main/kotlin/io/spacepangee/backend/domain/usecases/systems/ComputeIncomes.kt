package io.spacepangee.backend.domain.usecases.systems

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.errors.UseCaseInvalid
import io.spacepangee.backend.domain.models.NationIdentifier
import io.spacepangee.backend.domain.models.Wealth
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.repositories.ReadNations
import io.spacepangee.backend.domain.repositories.ReadStarSystems
import io.vavr.control.Either

fun List<StarSystem>.computesSystemsIncomes() = this.map {
    it.development * it.type
}

fun List<Wealth>.sums() = this.fold(Wealth.empty()) {
    wealth, addedValue -> wealth + addedValue
}

class ComputeIncomes(
    private val readNations: ReadNations,
    private val readStarSystems: ReadStarSystems
) {
    operator fun invoke(id: NationIdentifier): Either<SpacePangeeError, Wealth> =
        readNations.byId(id)
            .flatMap { readStarSystems.all(it.systems) }
            .map { it.computesSystemsIncomes() }
            .map { it.sums() }
}