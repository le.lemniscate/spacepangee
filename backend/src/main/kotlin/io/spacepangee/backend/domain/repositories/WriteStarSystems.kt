package io.spacepangee.backend.domain.repositories

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.vavr.control.Either

interface WriteStarSystems {
    fun save(system: StarSystem): Either<SpacePangeeError, StarSystem>
}