package io.spacepangee.backend.domain.models

class Wealth(
    val value: Int
) {
    companion object Factories {
        fun empty() = Wealth(0)
        fun of(value: Int) = Wealth(value)
    }

    operator fun plus(addedValue: Int) = Wealth(value + addedValue)
    operator fun plus(other: Wealth) = Wealth(value + other.value)
}