package io.spacepangee.backend.domain.errors

open class SpacePangeeError: Exception()
