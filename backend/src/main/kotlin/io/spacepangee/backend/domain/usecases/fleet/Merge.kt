package io.spacepangee.backend.domain.usecases.fleet

import io.spacepangee.backend.domain.errors.InvalidTarget
import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.errors.UseCaseInvalid
import io.spacepangee.backend.domain.models.assets.Fleet
import io.spacepangee.backend.domain.models.assets.FloatPoints
import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.models.assets.SpaceFloatIdentifier
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.repositories.ReadFleet
import io.spacepangee.backend.domain.repositories.ReadStarSystems
import io.spacepangee.backend.domain.repositories.WriteFleet
import io.spacepangee.backend.domain.repositories.WriteStarSystems
import io.vavr.control.Either

data class MergeData(
    val old: Fleet,
    val new: SpaceFloat,
    val system: StarSystem
) {
    fun movesNewFloatToSystem(): MergeData =
        copy(
            system = system.copy(fleet = system.fleet + new)
        )

    fun removesOldFleetFromSystem(): MergeData =
        copy(
            system = system.copy(fleet = system.fleet - old)
        )
}

class Merge(
    private val reader: ReadFleet,
    private val writer: WriteFleet,
    private val systemReader: ReadStarSystems,
    private val systemWriter: WriteStarSystems
) {
    operator fun invoke(firstFloat: SpaceFloatIdentifier, secondFloat: SpaceFloatIdentifier): Either<SpacePangeeError, SpaceFloat> {
        return reader.getAll(firstFloat, secondFloat)
            .flatMap { checkFleetHaveSameOwner(it) }
            .flatMap { checkFleetAreOnSameSystem(it) }
            .flatMap { createData(it) }
            .flatMap { saveNewFloat(it) }
            .map { it.movesNewFloatToSystem() }
            .flatMap { removeOldFleet(it) }
    }

    private fun checkFleetHaveSameOwner(fleet: List<SpaceFloat>): Either<SpacePangeeError, List<SpaceFloat>> =
        if (fleet.map { it.owner }.toSet().size != 1) {
            Either.left(InvalidTarget("Fleet must have the same owner to be merged"))
        } else {
            Either.right(fleet)
        }

    private fun checkFleetAreOnSameSystem(fleet: List<SpaceFloat>): Either<SpacePangeeError, List<SpaceFloat>> =
        (systemReader orbitedBy fleet)
            .flatMap {
                if (it.toSet().size != 1) {
                    Either.left(InvalidTarget("Fleet must be on the same system to be merged"))
                } else {
                    Either.right(fleet)
                }
            }

    private fun createData(fleet: List<SpaceFloat>): Either<SpacePangeeError, MergeData> {
        return systemReader.orbitedBy(fleet.first()).map {
            MergeData(
                Fleet.from(fleet.map { it.id }),
                fleet.fold(SpaceFloat.empty().copy(floatPoints = FloatPoints.from(0))) { newFloat, toMerge -> newFloat.copy(
                    floatPoints = newFloat.floatPoints + toMerge.floatPoints,
                ) },
                it
            )
        }
    }

    private fun saveNewFloat(data: MergeData): Either<SpacePangeeError, MergeData> =
        writer.save(data.new)
            .map { data.copy(new = it) }

    private fun removeOldFleet(data: MergeData): Either<SpacePangeeError, SpaceFloat> =
        writer.delete(data.old)
            .map {
                data.removesOldFleetFromSystem()
            }.flatMap {
                systemWriter.save(it.system)
            }.map {
                data.new
            }
}