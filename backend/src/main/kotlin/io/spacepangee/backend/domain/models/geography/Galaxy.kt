package io.spacepangee.backend.domain.models.geography

data class Galaxy(
    val nodes: List<GalaxyNode>
) {
}