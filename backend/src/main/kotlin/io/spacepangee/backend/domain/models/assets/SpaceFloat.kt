package io.spacepangee.backend.domain.models.assets

import io.spacepangee.backend.domain.models.NationIdentifier
import io.spacepangee.backend.domain.models.common.PositiveInt
import io.spacepangee.backend.domain.models.geography.systems.StarSystem

data class SpaceFloat(
    val id: SpaceFloatIdentifier,
    val owner: NationIdentifier,
    val jumpsLeft: PositiveInt,
    val floatPoints: FloatPoints
) {
    companion object Factories {
        fun empty() = SpaceFloat(
            SpaceFloatIdentifier.empty(),
            NationIdentifier.empty(),
            PositiveInt.zero(),
            FloatPoints.ofNewFloat()
        )
    }

    infix fun orbits(system: StarSystem) =
        system.fleet contains this
}