package io.spacepangee.backend.domain.repositories

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.assets.Fleet
import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.vavr.control.Either

interface WriteFleet {
    fun save(float: SpaceFloat): Either<SpacePangeeError, SpaceFloat>
    fun delete(fleet: Fleet): Either<SpacePangeeError, Unit>
}