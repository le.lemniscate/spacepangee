package io.spacepangee.backend.domain.errors

class AggregateRootNotFound: SpacePangeeError() {
}