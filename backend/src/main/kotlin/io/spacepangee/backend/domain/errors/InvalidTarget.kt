package io.spacepangee.backend.domain.errors

class InvalidTarget(
    val reason: String
): SpacePangeeError() {
}