package io.spacepangee.backend.domain.models.geography

import io.spacepangee.backend.domain.models.geography.systems.SystemIdentifier

data class GalaxyNode(
    val id: SystemIdentifier,
    val linkTo: List<SystemIdentifier>
)
