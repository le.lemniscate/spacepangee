package io.spacepangee.backend.domain.models.geography

import io.spacepangee.backend.domain.models.geography.systems.SystemIdentifier

data class StarPath(val content: List<SystemIdentifier>) {
    companion object Factories {
        fun empty() = StarPath(listOf())
    }

    val length
        get() = content.size
}
