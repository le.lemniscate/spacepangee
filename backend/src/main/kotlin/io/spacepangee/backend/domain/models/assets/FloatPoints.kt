package io.spacepangee.backend.domain.models.assets

import io.spacepangee.backend.domain.models.common.PositiveInt

data class FloatPoints(val value: PositiveInt) {
    companion object Factories {
        fun from(value: Int) = FloatPoints(PositiveInt.from(value))
        fun ofNewFloat() = from(1)
    }

    operator fun plus(other: FloatPoints) =
        FloatPoints(value + other.value)
}