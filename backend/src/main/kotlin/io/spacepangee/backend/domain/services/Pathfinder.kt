package io.spacepangee.backend.domain.services

import io.spacepangee.backend.domain.models.geography.StarPath
import io.spacepangee.backend.domain.models.geography.systems.SystemIdentifier

interface Pathfinder {
    fun findShortestPath(system1: SystemIdentifier, system2: SystemIdentifier): StarPath

}