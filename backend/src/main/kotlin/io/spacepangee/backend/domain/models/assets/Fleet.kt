package io.spacepangee.backend.domain.models.assets

data class Fleet(
    val content: List<SpaceFloatIdentifier>
) {
    companion object Factories {
        fun empty() = Fleet(listOf())
        fun from(vararg ids: SpaceFloatIdentifier) = Fleet(ids.toList())
        fun from(ids: List<SpaceFloatIdentifier>) = Fleet(ids)
    }

    infix fun contains(spaceFloat: SpaceFloatIdentifier) = content.contains(spaceFloat)
    infix fun contains(spaceFloat: SpaceFloat) = contains(spaceFloat.id)

    fun first() = content.first()

    operator fun plus(fleet: Fleet) =
        copy(content = content + fleet.content)
    operator fun plus(spaceFloat: SpaceFloat) =
        copy(content = content + spaceFloat.id)

    operator fun minus(fleet: Fleet) =
        copy(content = content - fleet.content)
    operator fun minus(spaceFloat: SpaceFloat) =
        copy(content = content - spaceFloat.id)
}