package io.spacepangee.backend.domain.models.geography.systems

fun getMaxNodes() = 6

data class Nodes(
    val neighbors: StarSystems,
    val unexplored: Int
) {
    companion object Factories {
        fun empty() = Nodes(StarSystems.empty(), 0)
    }

    val fullyExplored
        get() = unexplored == 0
}