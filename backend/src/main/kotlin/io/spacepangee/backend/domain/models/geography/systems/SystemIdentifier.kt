package io.spacepangee.backend.domain.models.geography.systems

import io.spacepangee.backend.domain.models.common.Identifier
import java.util.*

data class SystemIdentifier(
    val value: String,
    val concrete: Boolean
): Identifier {
    companion object Factories {
        fun empty(): SystemIdentifier = SystemIdentifier("", false)
        fun of(value: String): SystemIdentifier = SystemIdentifier(value, true)
        fun of(uuid: UUID): SystemIdentifier = of(uuid.toString())
    }

    override fun isConcrete(): Boolean = concrete
}