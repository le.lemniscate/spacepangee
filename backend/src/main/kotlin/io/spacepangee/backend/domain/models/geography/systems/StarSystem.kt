package io.spacepangee.backend.domain.models.geography.systems

import io.spacepangee.backend.domain.models.NationIdentifier
import io.spacepangee.backend.domain.models.assets.Fleet
import io.spacepangee.backend.domain.models.assets.SpaceFloat

data class StarSystem(
    val id: SystemIdentifier,
    val type: Type,
    val development: Development,
    val state: SystemState,
    val owner: NationIdentifier,
    val nodes: Nodes,
    val fleet: Fleet
) {
    companion object Factories {
        fun empty() = StarSystem(
            SystemIdentifier.empty(),
            Type.undefined(),
            Development.undefined(),
            SystemState.NOT_EXPLORED,
            NationIdentifier.empty(),
            Nodes.empty(),
            Fleet.empty()
        )
    }

    val fullyExplored
        get() = nodes.fullyExplored

    operator fun plus(float: SpaceFloat) =
        copy(fleet = fleet + float)
    operator fun minus(float: SpaceFloat) =
        copy(fleet = fleet - float)

}
