package io.spacepangee.backend.domain.usecases.fleet

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.errors.UseCaseInvalid
import io.spacepangee.backend.domain.models.assets.SpaceFloatIdentifier
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.repositories.ReadFleet
import io.spacepangee.backend.domain.repositories.ReadStarSystems
import io.spacepangee.backend.domain.repositories.WriteStarSystems
import io.vavr.control.Either

class Explore(
    val starSystemReader: ReadStarSystems,
    val starSystemWriter: WriteStarSystems
) {
    operator fun invoke(floatTarget: SpaceFloatIdentifier): Either<SpacePangeeError, StarSystem> {
        return starSystemReader.orbitedBy(floatTarget)
            .flatMap { checkExplorationCapabilities(it) }
            .map { explore(it) }
            .flatMap { starSystemWriter.save(it) }
        // TODO : should randomize the target system
        // TODO : should also set as discovered all systems owned by other nations
    }

    private fun checkExplorationCapabilities(it: StarSystem): Either<SpacePangeeError, StarSystem> =
        if (it.fullyExplored) {
            Either.left(UseCaseInvalid())
        } else {
            Either.right(it)
        }

    private fun explore(it: StarSystem): StarSystem = // TODO: should check the integrity of the new values in the AR
        it.copy(
            nodes = it.nodes.copy(
                neighbors = it.nodes.neighbors + StarSystem.empty(),
                unexplored = it.nodes.unexplored - 1
            )
        )
}