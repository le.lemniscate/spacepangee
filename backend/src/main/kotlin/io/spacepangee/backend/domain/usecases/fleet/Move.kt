package io.spacepangee.backend.domain.usecases.fleet

import io.spacepangee.backend.domain.errors.NotEnoughJumpsLeft
import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.models.assets.SpaceFloatIdentifier
import io.spacepangee.backend.domain.models.geography.StarPath
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.models.geography.systems.SystemIdentifier
import io.spacepangee.backend.domain.repositories.ReadFleet
import io.spacepangee.backend.domain.repositories.ReadStarSystems
import io.spacepangee.backend.domain.repositories.WriteFleet
import io.spacepangee.backend.domain.repositories.WriteStarSystems
import io.spacepangee.backend.domain.services.Pathfinder
import io.vavr.control.Either

data class MovesDataset(
    val float: SpaceFloat,
    val pathfinder: Pathfinder,
    val start: StarSystem = StarSystem.empty(),
    val end: StarSystem = StarSystem.empty(),
    val path: StarPath = StarPath.empty()
) {
    fun startsAt(start: Either<SpacePangeeError, StarSystem>): Either<SpacePangeeError, MovesDataset> =
        start.map { copy(start = it) }

    fun endsAt(end: Either<SpacePangeeError, StarSystem>): Either<SpacePangeeError, MovesDataset> =
        end.map { copy(end = it) }

    fun findShortestPathTo(id: SystemIdentifier) =
        copy(path = pathfinder.findShortestPath(start.id, id))

    fun checksFloatHasEnoughJumpsLeft(): Either<SpacePangeeError, MovesDataset> =
        if (float.jumpsLeft >= path.length) {
            Either.right(this)
        } else {
            Either.left(NotEnoughJumpsLeft())
        }

    fun removesJumpsFromStartFloat() = copy(float = float.copy(jumpsLeft = float.jumpsLeft - path.length))

}

class Move(
    private val readFleet: ReadFleet,
    private val writeFleet: WriteFleet,
    private val readStarSystems: ReadStarSystems,
    val writeStarSystems: WriteStarSystems,
    private val pathfinder: Pathfinder
) {
    operator fun invoke(
        float: SpaceFloatIdentifier,
        system: SystemIdentifier
    ): Either<SpacePangeeError, StarSystem> {
        return readFleet.byId(float)
            .map { MovesDataset(it, pathfinder) }
            .flatMap { it.startsAt(readStarSystems orbitedBy it.float.id) }
            .flatMap { it.endsAt(readStarSystems byId system) }
            .map { it.findShortestPathTo(system) }
            .flatMap { it.checksFloatHasEnoughJumpsLeft() }
            .map { it.removesJumpsFromStartFloat() }
            .flatMap { saveFloat(it) }
            .flatMap { removeFloatFromStartSystem(it) } // TODO : reprise sur erreur
            .flatMap { addFloatToEndSystem(it) }
    }

    private fun saveFloat(dataset: MovesDataset): Either<SpacePangeeError, MovesDataset> =
        writeFleet.save(dataset.float)
            .map { dataset }

    private fun removeFloatFromStartSystem(dataset: MovesDataset): Either<SpacePangeeError, MovesDataset> =
        writeStarSystems.save(dataset.start - dataset.float)
            .map { dataset }

    private fun addFloatToEndSystem(dataset: MovesDataset): Either<SpacePangeeError, StarSystem> =
        writeStarSystems.save(dataset.end + dataset.float)
}