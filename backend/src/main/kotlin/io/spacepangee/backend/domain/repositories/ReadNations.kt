package io.spacepangee.backend.domain.repositories

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.Nation
import io.spacepangee.backend.domain.models.NationIdentifier
import io.vavr.control.Either

interface ReadNations {
    fun byId(id: NationIdentifier): Either<SpacePangeeError, Nation>
}