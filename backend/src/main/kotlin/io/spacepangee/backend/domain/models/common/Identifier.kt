package io.spacepangee.backend.domain.models.common

interface Identifier {
    fun isConcrete(): Boolean
}
