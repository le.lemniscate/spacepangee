package io.spacepangee.backend.domain.models.geography.systems

data class Type(val value: Int) {
    companion object Factories {
        fun undefined() = Type(-1)
        fun of(value: Int) = Type(value)
    }

    val undefined: Boolean
        get() = value == -1
}