package io.spacepangee.backend.domain.models.common

data class PositiveInt (
    val value: Int
) {
    companion object Factories {
        fun from(value: Int) = if (value > 0) {
            PositiveInt(value)
        } else {
            PositiveInt(0)
        }

        fun zero() = PositiveInt(0)
    }

    operator fun plus(i: PositiveInt) = this + i.value
    operator fun plus(i: Int) = copy(value = value + i)

    operator fun minus(i: PositiveInt) = this - i.value
    operator fun minus(i: Int) = if (this.value > i) {
        copy(value = value - i)
    } else {
        PositiveInt(0)
    }

    operator fun compareTo(b: PositiveInt) = value - b.value
    operator fun compareTo(b: Int) = value - b
    override operator fun equals(b: Any?): Boolean {
        return when (b) {
            is Int -> value == b
            is PositiveInt -> value == b.value
            else -> false
        }
    }
}