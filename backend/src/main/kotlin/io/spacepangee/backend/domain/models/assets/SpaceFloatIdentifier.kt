package io.spacepangee.backend.domain.models.assets

import io.spacepangee.backend.domain.models.common.Identifier
import java.util.*

class SpaceFloatIdentifier(
    val value: String,
    val concrete: Boolean
): Identifier {
    companion object Factories {
        fun empty() = SpaceFloatIdentifier("", false)
        fun of(value: String) = SpaceFloatIdentifier(value, true)
        fun of(value: UUID) = of(value.toString())
    }

    override fun isConcrete() = concrete
}