package io.spacepangee.backend.domain.repositories

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.models.assets.SpaceFloatIdentifier
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.models.geography.systems.StarSystems
import io.spacepangee.backend.domain.models.geography.systems.SystemIdentifier
import io.vavr.control.Either

interface ReadStarSystems {
    infix fun byId(id: SystemIdentifier): Either<SpacePangeeError, StarSystem>
    fun all(ids: StarSystems): Either<SpacePangeeError, List<StarSystem>>
    infix fun orbitedBy(floatId: SpaceFloatIdentifier): Either<SpacePangeeError, StarSystem>
    infix fun orbitedBy(float: SpaceFloat): Either<SpacePangeeError, StarSystem>
    infix fun orbitedBy(fleet: List<SpaceFloat>): Either<SpacePangeeError, List<StarSystem>>
}