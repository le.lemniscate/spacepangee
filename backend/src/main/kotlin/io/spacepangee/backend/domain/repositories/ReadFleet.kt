package io.spacepangee.backend.domain.repositories

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.assets.Fleet
import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.models.assets.SpaceFloatIdentifier
import io.vavr.control.Either

interface ReadFleet {
    fun byId(id: SpaceFloatIdentifier): Either<SpacePangeeError, SpaceFloat>
    fun getAll(vararg ids: SpaceFloatIdentifier): Either<SpacePangeeError, List<SpaceFloat>>
}
