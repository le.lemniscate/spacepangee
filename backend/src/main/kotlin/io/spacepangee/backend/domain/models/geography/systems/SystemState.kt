package io.spacepangee.backend.domain.models.geography.systems

enum class SystemState {
    NOT_EXPLORED,
    EXPLORED
}