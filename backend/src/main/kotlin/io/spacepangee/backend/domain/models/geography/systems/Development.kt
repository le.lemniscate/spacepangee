package io.spacepangee.backend.domain.models.geography.systems

import io.spacepangee.backend.domain.models.Wealth

data class Development(val value: Int) {
    companion object Factories {
        fun undefined() = Development(-1)
        fun of(value: Int) = Development(value)
    }

    val undefined: Boolean
        get() = value == -1

    operator fun times(type: Type): Wealth = Wealth.of(value * type.value)
}