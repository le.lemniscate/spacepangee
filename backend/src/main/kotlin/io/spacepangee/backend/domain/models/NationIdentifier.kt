package io.spacepangee.backend.domain.models

import io.spacepangee.backend.domain.models.common.Identifier
import java.util.*

data class NationIdentifier(
    val value: String,
    val concrete: Boolean
): Identifier {
    companion object Factories {
        fun empty() = NationIdentifier("", false)
        fun of(value: String) = NationIdentifier(value, true)
        fun of(value: UUID) = of(value.toString())
    }

    override fun isConcrete() = concrete
}
