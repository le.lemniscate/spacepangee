package io.spacepangee.backend.domain.models.geography.systems

data class StarSystems(
    val content: List<SystemIdentifier>
) {
    companion object Factories {
        fun empty() = StarSystems(listOf())
        fun of(ids: List<SystemIdentifier>) = StarSystems(ids)
        fun from(systems: List<StarSystem>) = of(systems.map { it.id })
    }

    operator fun plus(system: StarSystem) =
        StarSystems.of(content + system.id)
}