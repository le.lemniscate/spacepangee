package io.spacepangee.backend.domain.models

import io.spacepangee.backend.domain.models.assets.Fleet
import io.spacepangee.backend.domain.models.geography.systems.StarSystems

data class Nation(
    val id: NationIdentifier,
    val systems: StarSystems,
    val fleet: Fleet
) {
    companion object Factories {
        fun empty(): Nation = Nation(
            NationIdentifier.empty(),
            StarSystems.empty(),
            Fleet.empty()
        )
    }
}
