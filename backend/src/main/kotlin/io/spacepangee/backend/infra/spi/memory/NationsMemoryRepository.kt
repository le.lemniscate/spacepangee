package io.spacepangee.backend.infra.spi.memory

import io.spacepangee.backend.domain.errors.AggregateRootNotFound
import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.Nation
import io.spacepangee.backend.domain.models.NationIdentifier
import io.spacepangee.backend.domain.repositories.ReadNations
import io.spacepangee.backend.domain.repositories.WriteNations
import io.vavr.control.Either
import java.util.*

class NationsMemoryRepository: ReadNations, WriteNations {
    private val content = mutableMapOf<NationIdentifier, Nation>()

    override fun byId(id: NationIdentifier): Either<SpacePangeeError, Nation> =
        if (content.containsKey(id)) {
            Either.right(content.get(id))
        } else {
            Either.left(AggregateRootNotFound())
        }

    override fun save(nation: Nation): Either<SpacePangeeError, Nation> {
        var add = nation
        if (!nation.id.concrete) {
            add = nation.copy(id = NationIdentifier.of(UUID.randomUUID()))
        }
        content[add.id] = add
        return Either.right(add)
    }
}