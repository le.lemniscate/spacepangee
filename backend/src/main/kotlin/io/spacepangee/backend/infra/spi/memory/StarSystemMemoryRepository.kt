package io.spacepangee.backend.infra.spi.memory

import io.spacepangee.backend.domain.errors.AggregateRootNotFound
import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.models.assets.SpaceFloatIdentifier
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.models.geography.systems.StarSystems
import io.spacepangee.backend.domain.models.geography.systems.SystemIdentifier
import io.spacepangee.backend.domain.repositories.ReadStarSystems
import io.spacepangee.backend.domain.repositories.WriteStarSystems
import io.spacepangee.backend.utils.toEitherOfList
import io.vavr.control.Either
import java.util.*
import kotlin.NoSuchElementException

class StarSystemMemoryRepository: ReadStarSystems, WriteStarSystems {
    private val content = mutableMapOf<SystemIdentifier, StarSystem>()

    override fun byId(id: SystemIdentifier): Either<SpacePangeeError, StarSystem> =
        if (content.containsKey(id)) {
            Either.right(content[id])
        } else {
            Either.left(AggregateRootNotFound())
        }

    override fun all(ids: StarSystems): Either<SpacePangeeError, List<StarSystem>> =
        Either.right(
            ids.content.mapNotNull { content[it] }
        )

    override fun orbitedBy(floatId: SpaceFloatIdentifier): Either<SpacePangeeError, StarSystem> {
        return try {
            Either.right(content.values.first { it.fleet.contains(floatId) })
        } catch (e: NoSuchElementException) {
            Either.left(AggregateRootNotFound())
        }
    }

    override fun orbitedBy(float: SpaceFloat): Either<SpacePangeeError, StarSystem> = orbitedBy(float.id)
    override fun orbitedBy(fleet: List<SpaceFloat>): Either<SpacePangeeError, List<StarSystem>> =
        fleet.map { this orbitedBy it }
            .toEitherOfList()

    override fun save(system: StarSystem): Either<SpacePangeeError, StarSystem> {
        var add = system
        if (!system.id.concrete) {
            add = system.copy(id = SystemIdentifier.of(UUID.randomUUID()))
        }
        content[add.id] = add
        return Either.right(add)
    }
}