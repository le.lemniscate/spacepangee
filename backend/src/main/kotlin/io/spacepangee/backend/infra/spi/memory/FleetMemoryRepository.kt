package io.spacepangee.backend.infra.spi.memory

import io.spacepangee.backend.domain.errors.AggregateRootNotFound
import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.assets.Fleet
import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.models.assets.SpaceFloatIdentifier
import io.spacepangee.backend.domain.repositories.ReadFleet
import io.spacepangee.backend.domain.repositories.WriteFleet
import io.vavr.control.Either
import java.util.*

class FleetMemoryRepository: WriteFleet, ReadFleet {
    private val content = mutableMapOf<SpaceFloatIdentifier, SpaceFloat>()

    override fun save(float: SpaceFloat): Either<SpacePangeeError, SpaceFloat> {
        var add = float
        if (!float.id.concrete) {
            add = float.copy(id = SpaceFloatIdentifier.of(UUID.randomUUID()))
        }
        content[add.id] = add
        return Either.right(add)
    }

    override fun delete(fleet: Fleet): Either<SpacePangeeError, Unit> =
        if (fleet.content.all { content.containsKey(it) }) {
            fleet.content.forEach { content.remove(it) }
            Either.right(Unit.apply {})
        } else {
            Either.left(AggregateRootNotFound())
        }

    override fun byId(id: SpaceFloatIdentifier): Either<SpacePangeeError, SpaceFloat> {
        val result = content[id]
        return if (result != null) {
            Either.right(result)
        } else {
            Either.left(AggregateRootNotFound())
        }
    }

    override fun getAll(vararg ids: SpaceFloatIdentifier): Either<SpacePangeeError, List<SpaceFloat>> {
        val all = ids.map { byId(it) }
        return if (all.any { it.isLeft }) {
            Either.left(AggregateRootNotFound())
        } else {
            Either.right(all.map { it.get() })
        }
    }
}