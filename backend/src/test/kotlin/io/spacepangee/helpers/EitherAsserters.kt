package io.spacepangee.helpers

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.vavr.control.Either
import org.assertj.core.api.Assertions.*
import org.assertj.core.api.ObjectAssert
import org.assertj.core.api.ThrowableAssert

data class EitherAsserter<T>(
    val either: Either<SpacePangeeError, T>
) {
    val isLeft: ThrowableAssert
        get() {
            assertThat(either.isLeft).isTrue
            return assertThat(either.left)
        }

    val isRight: ObjectAssert<T>
        get() {
            assertThat(either.isRight).isTrue
            return assertThat(either.get())
        }
}

fun <T> assert(either: Either<SpacePangeeError, T>) =
    EitherAsserter(either)

