package io.spacepangee.backend.domain.usecases.fleet.dsl.merge

import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.TargetToken

class MergeTestsContext(
    private val repositories: Repositories
) {

    fun float(token: TargetToken) = repositories.fleetRepository.get(token)

}