package io.spacepangee.backend.domain.usecases.fleet.dsl.explore

import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.models.assets.SpaceFloatIdentifier
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.usecases.dsl.Performer
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.domain.usecases.fleet.Explore
import io.spacepangee.backend.infra.spi.memory.default
import io.spacepangee.backend.infra.spi.memory.none

class ExplorePerformer(
    repositories: Repositories,
    private val context: ExploreTestsContext
): Performer<StarSystem> {

    // TODO: should not link toward the contextual repo in the performers
    val explore = Explore(repositories.starSystemRepository, repositories.starSystemRepository)

    lateinit var floatTarget: SpaceFloatIdentifier

    override fun invoke() = explore(floatTarget)

    fun `a non existing space float`(): SpaceFloat {
        context.setFloatAsPerformer(none)
        floatTarget = context.floatTarget.id
        return context.floatTarget
    }

    fun `the space float`(target: TargetToken = default): SpaceFloat {
        context.setFloatAsPerformer(target)
        floatTarget = context.floatTarget.id
        return context.floatTarget
    }

    fun `the space float of`(target: TargetToken): SpaceFloat {
        context.setFloatAsPerformer(context.nation(target).fleet.first())
        floatTarget = context.floatTarget.id
        return context.floatTarget
    }

}