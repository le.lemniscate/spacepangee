package io.spacepangee.backend.domain.usecases.systems

import io.spacepangee.backend.domain.usecases.systems.dsl.computeincomes.`computing income for`
import org.junit.jupiter.api.Test

class ComputeIncomesTests {
    @Test
    fun `error 1`() {
        `computing income for` {
            `a non existing nation`()
        } `results in` {
            `an error`()
        }
    }

    @Test
    fun `simple income computation`() {
        `computing income for` {
            `a nation` {
                `owning a random system`()
            }
        } `results in`{
            `system's` { type times development }
        }
    }

    @Test
    fun `multiple systems income computation`() {
        `computing income for` {
            `a nation` {
                `owning multiple systems`()
            }
        } `results in`{
            `a sum of all systems` { type times development }
        }
    }
}