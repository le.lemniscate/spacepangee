package io.spacepangee.backend.domain.models.generators

import io.spacepangee.backend.domain.models.NationIdentifier
import java.util.*

fun NationIdentifier.Factories.random() = of(UUID.randomUUID())
