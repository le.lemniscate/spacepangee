package io.spacepangee.backend.domain.usecases.dsl.initialiers

import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.services.HardcodedPathfinder
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.domain.usecases.dsl.wrappers.IntWrapper
import io.spacepangee.backend.infra.spi.memory.ParametersStore
import io.spacepangee.backend.infra.spi.memory.StarSystemContextualizedRepository
import io.spacepangee.backend.infra.spi.memory.default

class SystemConstraintInitializer(
    private val systemContextualizedRepository: StarSystemContextualizedRepository,
    private val pathfinder: HardcodedPathfinder,
    private val intParametersStore: ParametersStore<IntWrapper>
) {
    var system = StarSystem.empty()
    var targetToken = default

    fun atADistanceOf(getIntWrapper: ParametersStore<IntWrapper>.() -> IntWrapper) {
        pathfinder.setLength(intParametersStore.getIntWrapper().value)
    }

    fun atADistanceOf(target: TargetToken, getIntWrapper: ParametersStore<IntWrapper>.() -> IntWrapper) {
        val wrapper = intParametersStore.getIntWrapper()
        intParametersStore[target] = wrapper
        pathfinder.setLength(wrapper.value)
    }

    fun unreachable() {
        pathfinder.setLength(20000)
    }

    fun called(target: TargetToken) {
        targetToken = target
    }

    fun save() = systemContextualizedRepository.save(targetToken, system).get()
}