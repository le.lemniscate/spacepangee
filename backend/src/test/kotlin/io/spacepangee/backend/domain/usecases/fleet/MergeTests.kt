package io.spacepangee.backend.domain.usecases.fleet

import io.spacepangee.backend.domain.usecases.fleet.dsl.merge.given
import io.spacepangee.backend.infra.spi.memory.*
import org.junit.jupiter.api.Test

class MergeTests {

    @Test
    fun `error 1`() {
        given {
            `a random space float`(default)
        } `asking to merge` {
            default and none
        } `results in` {
            `an error`()
        }
    }

    @Test
    fun `error 2`() {
        given {
            `a nation`(foreign)
            `a random space float`(default) {
                on { `a random system`(default) }
            }
            `a random space float`(foreign) {
                `owned by`(foreign)
                on(default)
            }
        } `asking to merge` {
            default and foreign
        } `results in` {
            `an error`()
        }
    }

    @Test
    fun `error 3`() {
        given {
            `a random space float`(default) {
                on { `a random system`(alpha) }
            }
            `a random space float`(other) {
                on { `a random system`(beta) }
            }
        } `asking to merge` {
            default and other
        } `results in` {
            `an error`()
        }
    }

    @Test
    fun `base case`() {
        given {
            `a random space float`(default) {
                on { `a random system`(alpha) }
                `with float points`(X)
            }
            `a random space float`(other) {
                on(alpha)
                `with float points`(Y)
            }
        } `asking to merge` {
            default and other
        } `results in` {
            `a space float` {
                `with float points` { get(X) + get(Y) }
                on(alpha)
            }
            `fleet do not exist anymore` {
                default and other
            }
            `system`(alpha) {
                `not orbited by`{ default and other }
            }
        }
    }
}