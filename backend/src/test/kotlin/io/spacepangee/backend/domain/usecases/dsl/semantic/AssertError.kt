package io.spacepangee.backend.domain.usecases.dsl.semantic

import io.spacepangee.backend.domain.errors.PerformNotLaunchedError
import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.vavr.control.Either

interface AssertError {
    fun `an error`()
}

class ErrorAsserter<R>(
    private val result: Either<SpacePangeeError, R>
): AssertError {
    override fun `an error`() {
        io.spacepangee.helpers.assert(result).isLeft
            .isNotInstanceOf(PerformNotLaunchedError::class.java)
    }
}
