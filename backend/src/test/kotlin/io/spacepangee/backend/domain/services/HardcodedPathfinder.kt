package io.spacepangee.backend.domain.services

import io.spacepangee.backend.domain.models.generators.geography.systems.random
import io.spacepangee.backend.domain.models.geography.StarPath
import io.spacepangee.backend.domain.models.geography.systems.SystemIdentifier

class HardcodedPathfinder(
    private var pathLength: Int
): Pathfinder {

    companion object Factories {
        fun ofLength(pathLength: Int) = HardcodedPathfinder(pathLength)
    }

    fun setLength(value: Int) {
        pathLength = value
    }

    override fun findShortestPath(system1: SystemIdentifier, system2: SystemIdentifier): StarPath =
        StarPath((0 until pathLength).map { SystemIdentifier.random() })
}