package io.spacepangee.backend.domain.usecases.dsl.wrappers

import io.spacepangee.backend.domain.models.geography.systems.StarSystem

class StarSystemsWrapper(
    systems: List<StarSystem>
) {
    private val innerWrappers = systems.map { StarSystemWrapper(it) }

    fun sum(compute: StarSystemWrapper.() -> IntWrapper): IntWrapper =
        innerWrappers.map { it.compute() }
            .fold(IntWrapper(0)) { acc, value -> acc + value }
}