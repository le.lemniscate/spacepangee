package io.spacepangee.backend.domain.usecases.dsl.semantic.assert

import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import org.assertj.core.api.Assertions.*

interface AssertSystem {
    fun system(target: TargetToken, assert: SystemAssertions.() -> Unit)
}

class SystemAsserter(
    private val repositories: Repositories
): AssertSystem {
    override fun system(target: TargetToken, assert: SystemAssertions.() -> Unit) {
        SystemAssertions(repositories, repositories.starSystemRepository.get(target)).assert()
    }
}

class SystemAssertions(
    private val repositories: Repositories,
    private val system: StarSystem
) {
    fun `not orbited by`(fleet: () -> Pair<TargetToken, TargetToken>) {
        val fleetIds = fleet().toList().map { repositories.fleetRepository.get(it).id }
        fleetIds.forEach { assertThat(system.fleet.contains(it)).isFalse }
    }
}
