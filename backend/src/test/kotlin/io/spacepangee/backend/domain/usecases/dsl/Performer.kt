package io.spacepangee.backend.domain.usecases.dsl

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.vavr.control.Either

interface Performer<R> {
    operator fun invoke(): Either<SpacePangeeError, R>
}