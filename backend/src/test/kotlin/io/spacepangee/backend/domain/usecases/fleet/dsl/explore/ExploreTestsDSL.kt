package io.spacepangee.backend.domain.usecases.fleet.dsl.explore

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.Nation
import io.spacepangee.backend.domain.models.assets.Fleet
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.usecases.dsl.DSL
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.domain.usecases.dsl.UnitTestRepositories
import io.spacepangee.backend.domain.usecases.dsl.initialiers.NationInitializer
import io.spacepangee.backend.domain.usecases.dsl.semantic.DeclareNation
import io.spacepangee.backend.domain.usecases.dsl.semantic.NationDeclarator
import io.spacepangee.backend.infra.spi.memory.*
import io.vavr.control.Either

fun Fleet.first() = content.first()

fun given(initialize: ExploreTestsDSL.() -> Unit) = ExploreTestsDSL(UnitTestRepositories()).given(initialize)

fun asking(initialize: ExplorePerformer.() -> Unit): ExploreTestsDSL
    = given {}.asking(initialize)

class ExploreTestsDSL(
    val repositories: Repositories
): DSL<
        ExploreTestsDSL,
        ExploreTestsContext,
        ExplorePerformer,
        StarSystem,
        ExploreAsserter
>(), DeclareNation by NationDeclarator(repositories) {
    override var dsl = this
    override lateinit var context: ExploreTestsContext
    override lateinit var performer: ExplorePerformer

    override fun initContext() = ExploreTestsContext(repositories)

    override fun initPerformer(context: ExploreTestsContext) =
        ExplorePerformer(repositories, context)

    override fun initAsserter(context: ExploreTestsContext, result: Either<SpacePangeeError, StarSystem>) =
        ExploreAsserter(repositories, context, result)

    infix fun `to explore results in`(assert: ExploreAsserter.() -> Unit) = then(assert)
}