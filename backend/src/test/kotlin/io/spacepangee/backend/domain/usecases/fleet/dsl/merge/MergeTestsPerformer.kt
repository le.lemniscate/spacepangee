package io.spacepangee.backend.domain.usecases.fleet.dsl.merge

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.models.assets.SpaceFloatIdentifier
import io.spacepangee.backend.domain.repositories.ReadFleet
import io.spacepangee.backend.domain.repositories.ReadStarSystems
import io.spacepangee.backend.domain.repositories.WriteFleet
import io.spacepangee.backend.domain.repositories.WriteStarSystems
import io.spacepangee.backend.domain.usecases.dsl.Performer
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.domain.usecases.fleet.Merge
import io.vavr.control.Either

class MergeTestsPerformer(
    readFleet: ReadFleet,
    writeFleet: WriteFleet,
    readSystem: ReadStarSystems,
    writeSystem: WriteStarSystems,
    private val context: MergeTestsContext
): Performer<SpaceFloat> {
    private val merge = Merge(readFleet, writeFleet, readSystem, writeSystem)
    private var firstFloat = SpaceFloatIdentifier.empty()
    private var secondFloat = SpaceFloatIdentifier.empty()

    override fun invoke(): Either<SpacePangeeError, SpaceFloat> =
        merge(firstFloat, secondFloat)

    fun setTargets(values: Pair<TargetToken, TargetToken>) {
        firstFloat = context.float(values.first).id
        secondFloat = context.float(values.second).id
    }
}