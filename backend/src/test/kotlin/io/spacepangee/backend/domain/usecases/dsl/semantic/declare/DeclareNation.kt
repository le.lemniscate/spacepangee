package io.spacepangee.backend.domain.usecases.dsl.semantic

import io.spacepangee.backend.domain.models.Nation
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.domain.usecases.dsl.initialiers.NationInitializer
import io.spacepangee.backend.infra.spi.memory.default

interface DeclareNation {
    infix fun `a nation`(initialize: NationInitializer.() -> Nation)
    infix fun `a nation`(token: TargetToken)
    fun `a nation`(token: TargetToken, initialize: NationInitializer.() -> Nation)
}

class NationDeclarator(val repositories: Repositories): DeclareNation {
    override infix fun `a nation`(initialize: NationInitializer.() -> Nation) {
        `a nation`(default, initialize)
    }

    override infix fun `a nation`(token: TargetToken) {
        `a nation`(token, { nation })
    }

    override fun `a nation`(token: TargetToken, initialize: NationInitializer.() -> Nation) {
        val initializer = NationInitializer(repositories)
        repositories.nationRepository.save(token, initializer.initialize())
    }
}
