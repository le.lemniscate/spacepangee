package io.spacepangee.backend.domain.usecases.fleet.dsl.move

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.domain.usecases.dsl.semantic.AssertError
import io.spacepangee.backend.domain.usecases.dsl.semantic.ErrorAsserter
import io.spacepangee.backend.domain.usecases.dsl.wrappers.IntWrapper
import io.spacepangee.backend.infra.spi.memory.ParametersStore
import io.vavr.control.Either
import org.assertj.core.api.Assertions.*

class MoveAsserter(
    private val repositories: Repositories,
    private val context: MoveTestsContext,
    private val result: Either<SpacePangeeError, StarSystem>
): AssertError by ErrorAsserter(result) {
    fun `the space float being on`(target: TargetToken) {
        val savedFloat = repositories.fleetRepository.byId(context.floatTarget.id).get()
        val savedSystem = repositories.starSystemRepository.byId(context.system(target).id).get()
        assertThat(savedSystem.fleet.contains(savedFloat.id)).isTrue
    }

    fun `with jumps equals to`(compute: ParametersStore<IntWrapper>.() -> IntWrapper) {
        val savedFloat = repositories.fleetRepository.byId(context.floatTarget.id).get()
        assertThat(savedFloat.jumpsLeft.value).isEqualTo(repositories.intParameterStore.compute().value)
    }
}