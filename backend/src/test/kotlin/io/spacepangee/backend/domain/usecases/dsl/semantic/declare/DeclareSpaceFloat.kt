package io.spacepangee.backend.domain.usecases.dsl.semantic

import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.domain.usecases.dsl.initialiers.FloatInitializer

interface DeclareSpaceFloat {
    fun `a random space float`(target: TargetToken)
    fun `a random space float`(target: TargetToken, initializer: FloatInitializer.() -> Unit)
}

class SpaceFloatDeclarator(
    var repositories: Repositories
): DeclareSpaceFloat {
    override fun `a random space float`(target: TargetToken) =
        `a random space float`(target) {}

    override fun `a random space float`(target: TargetToken, initializer: FloatInitializer.() -> Unit) {
        FloatInitializer(repositories)
            .named(target)
            .apply(initializer)
            .save()
    }

}
