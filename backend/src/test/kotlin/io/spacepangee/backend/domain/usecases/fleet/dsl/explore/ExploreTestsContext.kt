package io.spacepangee.backend.domain.usecases.fleet.dsl.explore

import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.models.assets.SpaceFloatIdentifier
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.domain.usecases.dsl.initialiers.NationInitializer
import io.spacepangee.backend.infra.spi.memory.*

class ExploreTestsContext(
    private val repositories: Repositories
) {

    lateinit var floatTarget: SpaceFloat

    fun setFloatAsPerformer(token: TargetToken): SpaceFloat {
        floatTarget = if (token == none) {
            SpaceFloat.empty()
        } else {
            float(token)
        }
        return floatTarget
    }

    fun setFloatAsPerformer(target: SpaceFloatIdentifier): SpaceFloat {
        floatTarget = repositories.fleetRepository.byId(target).get()
        return floatTarget
    }

    fun nation(token: TargetToken = default) = repositories.nationRepository.get(token)
    fun float(token: TargetToken = default) = repositories.fleetRepository.get(token)
    fun system(token: TargetToken = default) = repositories.starSystemRepository.get(token)
    fun float(identifier: SpaceFloatIdentifier) = repositories.fleetRepository.byId(identifier)
}