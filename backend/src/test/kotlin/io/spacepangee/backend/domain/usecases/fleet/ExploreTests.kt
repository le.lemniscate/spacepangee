package io.spacepangee.backend.domain.usecases.fleet

import io.spacepangee.backend.domain.usecases.fleet.dsl.explore.asking
import io.spacepangee.backend.domain.usecases.fleet.dsl.explore.given
import io.spacepangee.backend.infra.spi.memory.foreign
import io.spacepangee.backend.infra.spi.memory.origin
import org.junit.jupiter.api.Test

class ExploreTests {

    @Test
    fun `error 1`() {
        asking {
            `a non existing space float`()
        } `to explore results in` {
            `an error`()
        }
    }
    // TODO: RBAC tests

    @Test
    fun `error 3`() {
        given {
            `a nation` {
                `owning a random space float on` {
                    `a random system` {
                        `fully explored`()
                    }
                }
            }
        } asking {
            `the space float`()
        } `to explore results in` {
            `an error`()
        }
    }

    @Test
    fun `error 4`() {
        given {
            `a nation` {
                `owning a random space float on` {
                    `a random system` {
                        `with unexplored nodes`()
                    }
                }
            }
            `a nation`(foreign) {
                `owning a random space float`()
            }
        } asking {
            `the space float of`(foreign)
        } `to explore results in` {
            `an error`()
        }
    }

    @Test
    fun `base case`() {
        given {
            `a nation` {
                `owning a random space float on` {
                    `a random system`(origin) {
                        `with unexplored nodes`()
                    }
                }
            }
        } asking {
            `the space float`()
        } `to explore results in` {
            `a star system being received`()
            `the space float is on the received system`()
            `the original system has one less unexplored node`()
            `the original system has one more linked system`()
        }
    }
}