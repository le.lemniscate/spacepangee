package io.spacepangee.backend.domain.usecases.dsl.wrappers

import io.spacepangee.backend.domain.models.geography.systems.StarSystem

class StarSystemWrapper(
    system: StarSystem
) {
    val type = IntWrapper(system.type.value)
    val development = IntWrapper(system.development.value)
}