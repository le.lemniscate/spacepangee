package io.spacepangee.backend.domain.models.generators.assets

import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.repositories.WriteFleet

fun SpaceFloat.Factories.create(writeFleet: WriteFleet): SpaceFloat =
    writeFleet.save(empty()).get()
