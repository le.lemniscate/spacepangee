package io.spacepangee.backend.domain.usecases.fleet

import io.spacepangee.backend.domain.usecases.fleet.dsl.move.asking
import io.spacepangee.backend.domain.usecases.fleet.dsl.move.given
import io.spacepangee.backend.infra.spi.memory.*
import org.junit.jupiter.api.Test

class MoveTests {

    @Test
    fun `error 1`() {
        asking {
            `a non existing space float`()
        } `to move towards` {
            `any system`()
        } `results in` {
            `an error`()
        }
    }

    @Test
    fun `error 2`() {
        given {
            `a nation` {
                `owning a random space float` {
                    on { `a random system`() }
                    `with no jumps left`()
                }
            }
        } asking {
            `the space float`()
        } `to move towards` {
            `any system`()
        } `results in` {
            `an error`()
        }
    }

    @Test
    fun `error 3`() {
        given {
            `a nation` {
                `owning a random space float` {
                    on { `a random system`() }
                    `with jumps left`(X)
                }
            }
        } asking {
            `the space float`()
        } `to move towards` {
            `any system`{ unreachable() }
        } `results in` {
            `an error`()
        }
    }

    @Test
    fun `error 4`() {
        given {
            `a nation` {
                `owning a random space float` {
                    on { `a random system`() }
                    `with jumps left`(X)
                }
            }
        } asking {
            `the space float`()
        } `to move towards` {
            `any system` {
                atADistanceOf { `more than`(X) }
            }
        } `results in` {
            `an error`()
        }
    }

    @Test
    fun `error 5`() {
        given {
            `a nation` {
                `owning a random space float` {
                    on { `a random system`() }
                    `with jumps left`(X)
                }
            }
            `a nation`(foreign) {
                `owning a random space float`()
            }
        } asking {
            `the space float of`(foreign)
        } `to move towards` {
            `any system`()
        } `results in` {
            `an error`()
        }
    }

    @Test
    fun `base case 1`() {
        given {
            `a nation` {
                `owning a random space float` {
                    on { `a random system`() }
                    `with jumps left`(X)
                }
            }
        } asking {
            `the space float`()
        } `to move towards` {
            `any system` {
                atADistanceOf(Y) { `more than 0 and less than`(X) }
                called(target)
            }
        } `results in` {
            `the space float being on`(target)
            `with jumps equals to` { get(X) - get(Y) }
        }
    }
}