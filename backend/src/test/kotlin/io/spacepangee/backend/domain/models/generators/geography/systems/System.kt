package io.spacepangee.backend.domain.models.generators.geography.systems

import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.models.geography.systems.Type
import io.spacepangee.backend.domain.models.geography.systems.Development
import io.spacepangee.backend.domain.models.geography.systems.SystemIdentifier
import java.util.*
import kotlin.random.Random

fun SystemIdentifier.Factories.random() = of(UUID.randomUUID())

fun Type.Factories.random() = of(Random.nextInt(7))

fun Development.Factories.random() = of(Random.nextInt(10))

fun StarSystem.Factories.random() = empty() // TODO: inconsistent with the Space Float
    .copy(
        id = SystemIdentifier.random(),
        type = Type.random(),
        development = Development.random()
    )
