package io.spacepangee.backend.domain.models.generators.geography.systems

import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.models.geography.systems.StarSystems
import io.spacepangee.backend.infra.spi.memory.StarSystemContextualizedRepository
import kotlin.random.Random

fun StarSystems.Factories.amount(writeStarSystems: StarSystemContextualizedRepository, amount: Int) =
    from(MutableList(amount) { writeStarSystems.save(StarSystem.empty()).get() })

fun StarSystems.Factories.randomAmount(writeStarSystems: StarSystemContextualizedRepository, max: Int) =
    amount(writeStarSystems, Random.nextInt(max))

