package io.spacepangee.backend.domain.usecases.dsl.initialiers

import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.models.generators.geography.systems.allExplored
import io.spacepangee.backend.domain.models.generators.geography.systems.random
import io.spacepangee.backend.domain.models.generators.geography.systems.someUnexplored
import io.spacepangee.backend.domain.models.geography.systems.Nodes
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.infra.spi.memory.StarSystemContextualizedRepository
import io.spacepangee.backend.infra.spi.memory.default

class SystemInitializer(
    val writeStarSystems: StarSystemContextualizedRepository
) {
    private var system = StarSystem.random()

    // TODO : "a random system" should be on a upper layer (it is the case for the "a random float" for example)
    fun `a random system`(subinit: SubSystemInitializer.() -> StarSystem = { system }): Unit {
        `a random system`(default, subinit)
    }

    fun `a random system`(targetToken: TargetToken, subinit: SubSystemInitializer.() -> StarSystem = { system }) {
        system = writeStarSystems.save(targetToken, SubSystemInitializer().subinit())
            .get()
    }

    fun `with a space float`(spaceFloat: SpaceFloat): SystemInitializer {
        system = system.copy(fleet = system.fleet + spaceFloat)
        return this
    }

    fun save() {
        writeStarSystems.save(system)
    }

    inner class SubSystemInitializer {
        fun `fully explored`(): StarSystem {
            system = system.copy(
                nodes = Nodes.allExplored(writeStarSystems)
            )
            return system
        }

        fun `with unexplored nodes`(): StarSystem {
            system = system.copy(
                nodes = Nodes.someUnexplored(writeStarSystems)
            )
            return system
        }
    }
}