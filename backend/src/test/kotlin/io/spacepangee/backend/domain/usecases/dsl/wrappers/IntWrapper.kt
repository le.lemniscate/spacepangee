package io.spacepangee.backend.domain.usecases.dsl.wrappers

import kotlin.random.Random

// TODO: globally... Wrapper useful?
data class IntWrapper( // TODO: really useful?
    val value: Int
) {
    companion object Factories {
        fun randomNotZero() = IntWrapper(Random.nextInt(1, 10000))
    }

    infix fun times(otherWrapper: IntWrapper) = IntWrapper(value * otherWrapper.value)
    operator fun plus(otherWrapper: IntWrapper) = IntWrapper(value + otherWrapper.value)
    operator fun minus(otherWrapper: IntWrapper) = IntWrapper(value - otherWrapper.value)
}