package io.spacepangee.backend.domain.usecases.fleet.dsl.move

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.services.HardcodedPathfinder
import io.spacepangee.backend.domain.usecases.dsl.DSL
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.UnitTestRepositories
import io.spacepangee.backend.domain.usecases.dsl.semantic.DeclareNation
import io.spacepangee.backend.domain.usecases.dsl.semantic.NationDeclarator
import io.vavr.control.Either

fun given(initialize: MoveTestsDSL.() -> Unit): MoveTestsDSL {
    val dsl = MoveTestsDSL(UnitTestRepositories())
    dsl.initialize()
    return dsl
}

fun asking(initialize: MovePerformer.() -> Unit): MoveTestsDSL = given {}.asking(initialize)

class MoveTestsDSL(
    val repositories: Repositories
): DSL<
        MoveTestsDSL,
        MoveTestsContext,
        MovePerformer,
        StarSystem,
        MoveAsserter
>(), DeclareNation by NationDeclarator(repositories) {
    override var dsl = this
    override lateinit var context: MoveTestsContext
    override lateinit var performer: MovePerformer

    infix fun `to move towards`(pointToSystem: MovePerformer.() -> Unit): MoveTestsDSL {
        performer.pointToSystem()
        return this
    }

    infix fun `results in`(assert: MoveAsserter.() -> Unit) = then(assert)

    override fun initContext(): MoveTestsContext =
        MoveTestsContext(repositories)

    override fun initPerformer(context: MoveTestsContext): MovePerformer =
        MovePerformer(repositories, HardcodedPathfinder(0), context)

    override fun initAsserter(context: MoveTestsContext, result: Either<SpacePangeeError, StarSystem>) =
        MoveAsserter(repositories, context, result)
}