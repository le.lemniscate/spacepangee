package io.spacepangee.backend.domain.usecases.systems.dsl.computeincomes

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.Nation
import io.spacepangee.backend.domain.models.Wealth
import io.spacepangee.backend.domain.usecases.dsl.Performer
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.initialiers.NationInitializer
import io.spacepangee.backend.domain.usecases.dsl.wrappers.IntWrapper
import io.spacepangee.backend.domain.usecases.systems.ComputeIncomes
import io.spacepangee.backend.infra.spi.memory.*
import io.vavr.control.Either

class ComputeIncomesPerformer(
    private val nationRepository: NationsContextualizedRepository,
    systemRepository: StarSystemContextualizedRepository,
    repositories: Repositories
): Performer<Wealth> {
    val usecase = ComputeIncomes(nationRepository, systemRepository)
    private val nationInitializer = NationInitializer(repositories)
    lateinit var nation: Nation

    override fun invoke(): Either<SpacePangeeError, Wealth> = usecase(nation.id)

    fun `a non existing nation`() {
        nation = Nation.empty()
    }

    fun `a nation`(init: NationInitializer.() -> Nation) {
        nation = nationRepository
            .save(nationInitializer.init())
            .get()
    }
}
