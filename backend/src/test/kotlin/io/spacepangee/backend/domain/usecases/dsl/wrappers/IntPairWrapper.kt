package io.spacepangee.backend.domain.usecases.dsl.wrappers

data class IntPairWrapper(val X: IntWrapper, val Y: IntWrapper)