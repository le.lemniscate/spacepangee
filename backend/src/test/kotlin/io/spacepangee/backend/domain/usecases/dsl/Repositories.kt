package io.spacepangee.backend.domain.usecases.dsl

import io.spacepangee.backend.domain.usecases.dsl.wrappers.IntWrapper
import io.spacepangee.backend.infra.spi.memory.*

interface Repositories {
    val nationRepository: NationsContextualizedRepository
    val starSystemRepository: StarSystemContextualizedRepository
    val fleetRepository: FleetContextualizedRepository
    val intParameterStore: ParametersStore<IntWrapper>
}

class UnitTestRepositories(
    override val nationRepository: NationsContextualizedRepository = NationsContextualizedRepository.from(
        NationsMemoryRepository()),
    override val starSystemRepository: StarSystemContextualizedRepository = StarSystemContextualizedRepository.from(
        StarSystemMemoryRepository()
    ),
    override val fleetRepository: FleetContextualizedRepository = FleetContextualizedRepository.from(
        FleetMemoryRepository()
    ),
    override val intParameterStore: ParametersStore<IntWrapper> = ParametersStore()
): Repositories
