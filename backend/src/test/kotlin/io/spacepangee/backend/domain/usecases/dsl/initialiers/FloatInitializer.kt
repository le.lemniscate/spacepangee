package io.spacepangee.backend.domain.usecases.dsl.initialiers

import io.spacepangee.backend.domain.models.assets.FloatPoints
import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.models.common.PositiveInt
import io.spacepangee.backend.domain.models.generators.assets.create
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.domain.usecases.dsl.wrappers.IntWrapper
import io.spacepangee.backend.infra.spi.memory.default

class FloatInitializer(
    private val repositories: Repositories
) {
    var float = SpaceFloat.create(repositories.fleetRepository)
    var targetToken = default

    infix fun on(initializeSystem: SystemInitializer.() -> Unit) {
        SystemInitializer(repositories.starSystemRepository)
            .apply(initializeSystem)
            .`with a space float`(float)
            .save()
    }

    fun named(target: TargetToken): FloatInitializer {
        targetToken = target
        return this
    }

    fun on(target: TargetToken) {
        val system = repositories.starSystemRepository.get(target)
        repositories.starSystemRepository.save(
            system
                .copy(fleet = system.fleet + float)
        )
    }

    fun `with no jumps left`() {
        float = float.copy(jumpsLeft = PositiveInt.zero())
    }

    fun `with jumps left`(target: TargetToken) {
        val jumps = IntWrapper.randomNotZero()
        repositories.intParameterStore[target] = jumps
        float = float.copy(jumpsLeft = PositiveInt.from(jumps.value))
    }

    fun `with float points`(target: TargetToken) {
        val floatPoints = IntWrapper.randomNotZero()
        repositories.intParameterStore[target] = floatPoints
        float = float.copy(floatPoints = FloatPoints.from(floatPoints.value))
    }

    fun `owned by`(target: TargetToken) {
        var nation = repositories.nationRepository.get(target)
        nation = repositories.nationRepository.save(target,
            nation.copy(fleet = nation.fleet + float)).get()
        float = float.copy(owner = nation.id)
    }

    fun save(): SpaceFloat {
        float = repositories.fleetRepository.save(targetToken, float).get()
        return float
    }
}