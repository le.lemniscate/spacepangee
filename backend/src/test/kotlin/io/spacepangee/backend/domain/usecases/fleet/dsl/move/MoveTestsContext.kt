package io.spacepangee.backend.domain.usecases.fleet.dsl.move

import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.infra.spi.memory.*

class MoveTestsContext(
    val repositories: Repositories
) {

    lateinit var floatTarget: SpaceFloat

    fun setFloatAsPerformer(token: TargetToken): SpaceFloat {
        floatTarget = if (token == none) {
            SpaceFloat.empty()
        } else {
            float(token)
        }
        return floatTarget
    }

    fun float(token: TargetToken = default) = repositories.fleetRepository.get(token)
    fun system(token: TargetToken = default) = repositories.starSystemRepository.get(token)
    fun nation(token: TargetToken = default) = repositories.nationRepository.get(token)
}