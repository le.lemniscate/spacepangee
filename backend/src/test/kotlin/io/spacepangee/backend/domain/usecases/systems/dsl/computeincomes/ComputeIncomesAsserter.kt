package io.spacepangee.backend.domain.usecases.systems.dsl.computeincomes

import io.spacepangee.backend.domain.errors.PerformNotLaunchedError
import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.Wealth
import io.spacepangee.backend.domain.usecases.dsl.wrappers.IntWrapper
import io.spacepangee.backend.domain.usecases.dsl.wrappers.StarSystemWrapper
import io.spacepangee.backend.domain.usecases.dsl.wrappers.StarSystemsWrapper
import io.vavr.control.Either
import org.assertj.core.api.Assertions

class ComputeIncomesAsserter(
    private val context: ComputeIncomesContext,
    private val result: Either<SpacePangeeError, Wealth>
) {
    fun `an error`() {
        assert(result.isLeft)
        assert(result.left !is PerformNotLaunchedError)
    }

    fun `system's`(compute: StarSystemWrapper.() -> IntWrapper) {
        assert(result.isRight)
        val calculator = StarSystemWrapper(context.system)
        result.map {
            Assertions.assertThat(it.value).isEqualTo(calculator.compute().value)
        }
    }

    fun `a sum of all systems`(compute: StarSystemWrapper.() -> IntWrapper) {
        assert(result.isRight)
        val calculator = StarSystemsWrapper(context.systems)
        result.map {
            Assertions.assertThat(it.value).isEqualTo(calculator.sum(compute).value)
        }
    }
}