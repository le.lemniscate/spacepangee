package io.spacepangee.backend.domain.usecases.fleet.dsl.move

import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.models.assets.SpaceFloatIdentifier
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.models.geography.systems.SystemIdentifier
import io.spacepangee.backend.domain.services.HardcodedPathfinder
import io.spacepangee.backend.domain.usecases.dsl.Performer
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.domain.usecases.fleet.dsl.explore.first
import io.spacepangee.backend.domain.usecases.dsl.initialiers.SystemConstraintInitializer
import io.spacepangee.backend.domain.usecases.fleet.Move
import io.spacepangee.backend.infra.spi.memory.default
import kotlin.random.Random

class MovePerformer(
    private val repositories: Repositories,
    private val pathfinder: HardcodedPathfinder,
    private val context: MoveTestsContext
): Performer<StarSystem> {

    val move = Move(repositories.fleetRepository, repositories.fleetRepository, repositories.starSystemRepository, repositories.starSystemRepository, pathfinder)
    private lateinit var floatTarget: SpaceFloatIdentifier
    lateinit var starSystemTarget: SystemIdentifier

    override fun invoke() = move(floatTarget, starSystemTarget)

    fun `a non existing space float`() {
        floatTarget = SpaceFloatIdentifier.empty()
    }

    fun `the space float of`(target: TargetToken) {
        floatTarget = context.nation(target).fleet.first()
    }

    fun `any system`(
        init: SystemConstraintInitializer.() -> Unit = {}
    ) {
        pathfinder.setLength(Random.nextInt(20000))
        starSystemTarget = SystemConstraintInitializer(
            repositories.starSystemRepository,
            pathfinder,
            repositories.intParameterStore
        )
            .apply(init)
            .save().id
    }

    fun `the space float`(target: TargetToken = default): SpaceFloat {
        context.setFloatAsPerformer(target)
        floatTarget = context.floatTarget.id
        return context.floatTarget
    }
}