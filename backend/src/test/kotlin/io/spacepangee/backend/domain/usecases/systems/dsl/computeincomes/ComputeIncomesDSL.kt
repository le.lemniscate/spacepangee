package io.spacepangee.backend.domain.usecases.systems.dsl.computeincomes

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.Wealth
import io.spacepangee.backend.domain.usecases.dsl.DSL
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.UnitTestRepositories
import io.spacepangee.backend.infra.spi.memory.*
import io.vavr.control.Either

fun `computing income for`(performing: ComputeIncomesPerformer.() -> Unit): ComputeIncomesDSL
    = ComputeIncomesDSL(UnitTestRepositories()).`computing income for`(performing)

class ComputeIncomesDSL(val repositories: Repositories): DSL<
        ComputeIncomesDSL,
        ComputeIncomesContext,
        ComputeIncomesPerformer,
        Wealth,
        ComputeIncomesAsserter
>() {
    override var performer = ComputeIncomesPerformer(
        NationsContextualizedRepository.from(NationsMemoryRepository()),
        repositories.starSystemRepository,
        repositories
    )
    override var dsl = this
    override var context = initContext()

    fun `computing income for`(performing: ComputeIncomesPerformer.() -> Unit) = asking(performing)

    infix fun `results in`(assert: ComputeIncomesAsserter.() -> Unit) = then(assert)

    override fun initContext() = ComputeIncomesContext.from(repositories.starSystemRepository)

    override fun initPerformer(context: ComputeIncomesContext) = performer

    override fun initAsserter(
        context: ComputeIncomesContext,
        result: Either<SpacePangeeError, Wealth>
    ) = ComputeIncomesAsserter(initContext(), result)
}

