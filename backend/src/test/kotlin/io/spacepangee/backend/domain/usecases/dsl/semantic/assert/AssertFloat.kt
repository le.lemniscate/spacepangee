package io.spacepangee.backend.domain.usecases.dsl.semantic.assert

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.domain.usecases.dsl.wrappers.IntWrapper
import io.spacepangee.backend.infra.spi.memory.ParametersStore
import io.vavr.control.Either
import org.assertj.core.api.Assertions.*

interface AssertFloat {
    fun `a space float`(assert: FloatAssertions.() -> Unit)
}

class FloatAsserter(
    private val repositories: Repositories,
    private val result: Either<SpacePangeeError, SpaceFloat>
): AssertFloat {
    override fun `a space float`(assert: FloatAssertions.() -> Unit) {
        FloatAssertions(repositories, result.get()).assert()
    }
}

class FloatAssertions(
    private val repositories: Repositories,
    private val float: SpaceFloat
) {
    fun `with float points`(getWrapper: ParametersStore<IntWrapper>.() -> IntWrapper) {
        assertThat(float.floatPoints.value).isEqualTo(repositories.intParameterStore.getWrapper().value)
    }

    fun on(target: TargetToken) {
        assertThat(float orbits repositories.starSystemRepository.get(target)).isTrue
    }
}
