package io.spacepangee.backend.domain.usecases.fleet.dsl.merge

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.usecases.dsl.DSL
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.domain.usecases.dsl.UnitTestRepositories
import io.spacepangee.backend.domain.usecases.dsl.semantic.DeclareNation
import io.spacepangee.backend.domain.usecases.dsl.semantic.DeclareSpaceFloat
import io.spacepangee.backend.domain.usecases.dsl.semantic.NationDeclarator
import io.spacepangee.backend.domain.usecases.dsl.semantic.SpaceFloatDeclarator
import io.vavr.control.Either

fun given(initialize: MergeTestsDSL.() -> Unit): MergeTestsDSL {
    val dsl = MergeTestsDSL(UnitTestRepositories())
    dsl.initialize()
    return dsl
}

class MergeTestsDSL(
    private val repositories: Repositories
): DSL<
        MergeTestsDSL,
        MergeTestsContext,
        MergeTestsPerformer,
        SpaceFloat,
        MergeAsserter
>(),
        DeclareSpaceFloat by SpaceFloatDeclarator(repositories),
        DeclareNation by NationDeclarator(repositories)
{
    override var dsl = this
    override lateinit var context: MergeTestsContext
    override lateinit var performer: MergeTestsPerformer

    override fun initContext(): MergeTestsContext =
        MergeTestsContext(repositories)

    override fun initPerformer(context: MergeTestsContext): MergeTestsPerformer =
        MergeTestsPerformer(
            repositories.fleetRepository,
            repositories.fleetRepository,
            repositories.starSystemRepository,
            repositories.starSystemRepository,
            context
        )

    override fun initAsserter(context: MergeTestsContext, result: Either<SpacePangeeError, SpaceFloat>): MergeAsserter =
        MergeAsserter(repositories, result)

    infix fun `asking to merge`(initTargets: () -> Pair<TargetToken, TargetToken>): MergeTestsDSL {
        super.asking {
            setTargets(initTargets())
        }
        return this
    }

    infix fun `results in`(assert: MergeAsserter.() -> Unit) = then(assert)
}