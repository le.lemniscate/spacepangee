package io.spacepangee.backend.domain.models.generators.assets

import io.spacepangee.backend.domain.models.assets.Fleet
import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.repositories.WriteFleet

fun Fleet.Factories.one(writer: WriteFleet): Fleet =
    writer.save(SpaceFloat.empty())
        .map { empty().copy(content = listOf(it.id)) }
        .get()
