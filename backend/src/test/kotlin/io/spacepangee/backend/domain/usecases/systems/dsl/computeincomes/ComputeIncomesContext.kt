package io.spacepangee.backend.domain.usecases.systems.dsl.computeincomes

import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.infra.spi.memory.StarSystemContextualizedRepository
import io.spacepangee.backend.infra.spi.memory.default
import java.lang.NullPointerException

data class ComputeIncomesContext(
    val system: StarSystem,
    val systems: List<StarSystem>
) {
    companion object Factories {
        fun from(systemContextualizedRepository: StarSystemContextualizedRepository) =
            ComputeIncomesContext(
                try {
                    systemContextualizedRepository.get(default)
                } catch (e: NullPointerException) {
                    StarSystem.empty()
                },
                systemContextualizedRepository.all()
            )
    }
}