package io.spacepangee.backend.domain.usecases.fleet.dsl.merge

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.domain.usecases.dsl.semantic.AssertError
import io.spacepangee.backend.domain.usecases.dsl.semantic.ErrorAsserter
import io.spacepangee.backend.domain.usecases.dsl.semantic.assert.AssertFloat
import io.spacepangee.backend.domain.usecases.dsl.semantic.assert.AssertSystem
import io.spacepangee.backend.domain.usecases.dsl.semantic.assert.FloatAsserter
import io.spacepangee.backend.domain.usecases.dsl.semantic.assert.SystemAsserter
import io.vavr.control.Either

class MergeAsserter(
    val repositories: Repositories,
    result: Either<SpacePangeeError, SpaceFloat>
):  AssertError by ErrorAsserter(result),
    AssertFloat by FloatAsserter(repositories, result),
    AssertSystem by SystemAsserter(repositories) {
    fun `fleet do not exist anymore`(fleetPair: () -> Pair<TargetToken, TargetToken>) {
        val pair = fleetPair()
        val ids = Pair(
            repositories.fleetRepository.get(pair.first).id,
            repositories.fleetRepository.get(pair.second).id
        )
        io.spacepangee.helpers.assert(repositories.fleetRepository.byId(ids.first))
            .isLeft
        io.spacepangee.helpers.assert(repositories.fleetRepository.byId(ids.second))
            .isLeft
    }
}
