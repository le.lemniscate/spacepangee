package io.spacepangee.backend.domain.usecases.dsl.initialiers

import io.spacepangee.backend.domain.models.Nation
import io.spacepangee.backend.domain.models.NationIdentifier
import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.models.generators.assets.create
import io.spacepangee.backend.domain.models.generators.geography.systems.random
import io.spacepangee.backend.domain.models.generators.random
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.models.geography.systems.StarSystems
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.wrappers.IntWrapper
import io.spacepangee.backend.infra.spi.memory.FleetContextualizedRepository
import io.spacepangee.backend.infra.spi.memory.ParametersStore
import io.spacepangee.backend.infra.spi.memory.StarSystemContextualizedRepository
import kotlin.random.Random

class NationInitializer(
    private val repositories: Repositories
) {
    var nation = Nation.empty().copy(id = NationIdentifier.random()) // TODO see if the initializers have all the same handling

    fun `owning a random system`(): Nation {
        val system = repositories.starSystemRepository.save(
            StarSystem.random().copy(owner = nation.id)
        ).get()
        nation = nation.copy(
            systems = nation.systems + system
        )
        return nation
    }

    fun `owning multiple systems`(): Nation {
        val numberOfSystems = Random.nextInt(3)
        val systems = (2..numberOfSystems)
            .map { StarSystem.random().copy(owner = nation.id) }
            .map { repositories.starSystemRepository.save(it) }
            .map { it.get() }
        nation = nation.copy(systems = StarSystems.from(systems))
        return nation
    }

    fun `owning a random space float on`(initialize: SystemInitializer.() -> Unit): Nation {
        val newFloat = repositories.fleetRepository.save(
            SpaceFloat.create(repositories.fleetRepository)
        ).get()
        SystemInitializer(repositories.starSystemRepository)
            .`with a space float`(newFloat)
            .apply(initialize)
            .save()
        nation = nation.copy(
            fleet = nation.fleet + newFloat
        )
        return nation
    }

    fun `owning a random space float`(): Nation {
        val newFloat = repositories.fleetRepository.save(
            SpaceFloat.create(repositories.fleetRepository)
        ).get()
        nation = nation.copy(
            fleet = nation.fleet + newFloat
        )
        return nation
    }

    fun `owning a random space float`(initialize: FloatInitializer.() -> Unit): Nation {
        val newFloat = FloatInitializer(repositories)
            .apply(initialize)
            .save()
        nation = nation.copy(
            fleet = nation.fleet + newFloat
        )
        return nation
    }
}