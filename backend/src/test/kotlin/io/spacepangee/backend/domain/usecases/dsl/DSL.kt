package io.spacepangee.backend.domain.usecases.dsl

import io.spacepangee.backend.domain.errors.PerformNotLaunchedError
import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.usecases.dsl.initialiers.NationInitializer
import io.spacepangee.backend.domain.usecases.dsl.wrappers.IntWrapper
import io.spacepangee.backend.infra.spi.memory.*
import io.vavr.control.Either

interface ResultContainer<R> {
    var result: Either<SpacePangeeError, R>
}

abstract class DSL<I, C, P: Performer<R>, R, A>: ResultContainer<R> {
    abstract var dsl: I
    abstract var context: C
    abstract var performer: P

    override var result: Either<SpacePangeeError, R> = Either.left(PerformNotLaunchedError())

    infix fun given(initialize: I.() -> Unit): I {
        dsl.initialize()
        return dsl
    }

    infix fun asking(target: P.() -> Unit): I {
        context = initContext()
        performer = initPerformer(context)
        performer.target()
        return dsl
    }

    infix fun then(assert: A.() -> Unit) {
        result = performer()
        initAsserter(context, result).assert()
    }

    abstract fun initContext(): C
    abstract fun initPerformer(context: C): P
    abstract fun initAsserter(context: C, result: Either<SpacePangeeError, R>): A
}