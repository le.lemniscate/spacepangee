package io.spacepangee.backend.domain.usecases.dsl

data class TargetToken(val name: String) {
    infix fun and(other: TargetToken): Pair<TargetToken, TargetToken> {
        val values = listOf(this, other).shuffled()
        return Pair(values[0], values[1])
    }
}
