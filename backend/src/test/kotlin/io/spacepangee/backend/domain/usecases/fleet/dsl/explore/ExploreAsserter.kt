package io.spacepangee.backend.domain.usecases.fleet.dsl.explore

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.usecases.dsl.Repositories
import io.spacepangee.backend.domain.usecases.dsl.semantic.AssertError
import io.spacepangee.backend.domain.usecases.dsl.semantic.ErrorAsserter
import io.spacepangee.backend.infra.spi.memory.origin
import io.vavr.control.Either
import org.assertj.core.api.Assertions.*

class ExploreAsserter(
    private val repositories: Repositories,
    val context: ExploreTestsContext,
    private val result: Either<SpacePangeeError, StarSystem>,
    errorAsserter: AssertError = ErrorAsserter(result)
): AssertError by errorAsserter {
    fun `a star system being received`() {
        assertThat(result.isRight).isTrue
    }

    fun `the space float is on the received system`() {
        result.map {
            assertThat(context.floatTarget orbits it)
        }
    }

    fun `the original system has one less unexplored node`() {
        val originalSystem = context.system(origin)
        val newOriginalSystem = repositories.starSystemRepository.byId(context.system(origin).id).get()
        assertThat(newOriginalSystem.nodes.unexplored).isEqualTo(originalSystem.nodes.unexplored - 1)
    }

    fun `the original system has one more linked system`() {
        val originalSystem = context.system(origin)
        val newOriginalSystem = repositories.starSystemRepository.byId(context.system(origin).id).get()
        assertThat(newOriginalSystem.nodes.neighbors.content.size)
            .isEqualTo(originalSystem.nodes.neighbors.content.size + 1)
    }
}