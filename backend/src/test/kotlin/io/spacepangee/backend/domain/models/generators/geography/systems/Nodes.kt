package io.spacepangee.backend.domain.models.generators.geography.systems

import io.spacepangee.backend.domain.models.geography.systems.Nodes
import io.spacepangee.backend.domain.models.geography.systems.StarSystems
import io.spacepangee.backend.domain.models.geography.systems.getMaxNodes
import io.spacepangee.backend.infra.spi.memory.StarSystemContextualizedRepository
import kotlin.random.Random

fun Nodes.Factories.allExplored(writeStarSystems: StarSystemContextualizedRepository) = Nodes(
    StarSystems.randomAmount(writeStarSystems, getMaxNodes()),
    0
)

fun Nodes.Factories.someUnexplored(writeStarSystems: StarSystemContextualizedRepository): Nodes {
    val amountUnexplored = Random.nextInt(1, getMaxNodes())
    val amountExplored = if (amountUnexplored < getMaxNodes()) {
        Random.nextInt(amountUnexplored, getMaxNodes())
    } else {
        0
    }
    return Nodes(
        StarSystems.amount(writeStarSystems, amountExplored - amountUnexplored),
        amountUnexplored
    )
}
