package io.spacepangee.backend.infra.spi.memory

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.assets.SpaceFloatIdentifier
import io.spacepangee.backend.domain.models.geography.systems.StarSystem
import io.spacepangee.backend.domain.models.geography.systems.StarSystems
import io.spacepangee.backend.domain.models.geography.systems.SystemIdentifier
import io.spacepangee.backend.domain.repositories.ReadStarSystems
import io.spacepangee.backend.domain.repositories.WriteStarSystems
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.vavr.control.Either

class StarSystemContextualizedRepository(
    private val writer: WriteStarSystems,
    private val reader: ReadStarSystems,
    private val systems: MutableMap<TargetToken, SystemIdentifier> = mutableMapOf()
): WriteStarSystems by writer, ReadStarSystems by reader, ContextualizedWriter<StarSystem>, Context<StarSystem> {

    val all = mutableListOf<StarSystem>()

    companion object Factories {
        fun from(delegate: StarSystemMemoryRepository) = StarSystemContextualizedRepository(delegate, delegate)
    }

    override fun save(token: TargetToken, system: StarSystem): Either<SpacePangeeError, StarSystem> =
        writer.save(system)
            .map {
                systems[token] = it.id
                all.add(it)
                it
            }

    override fun byId(id: SystemIdentifier): Either<SpacePangeeError, StarSystem> = reader.byId(id)
    override fun orbitedBy(floatId: SpaceFloatIdentifier): Either<SpacePangeeError, StarSystem> =
        reader.orbitedBy(floatId)
    override fun all(ids: StarSystems): Either<SpacePangeeError, List<StarSystem>> = reader.all(ids)

    override fun get(token: TargetToken) = reader.byId(systems[token]!!).get()
    override fun all(): List<StarSystem> = all
}