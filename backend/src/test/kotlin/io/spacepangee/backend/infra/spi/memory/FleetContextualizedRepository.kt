package io.spacepangee.backend.infra.spi.memory

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.assets.SpaceFloat
import io.spacepangee.backend.domain.models.assets.SpaceFloatIdentifier
import io.spacepangee.backend.domain.repositories.ReadFleet
import io.spacepangee.backend.domain.repositories.WriteFleet
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.vavr.control.Either

class FleetContextualizedRepository(
    private val writer: WriteFleet,
    private val reader: ReadFleet,
    private val fleet: MutableMap<TargetToken, SpaceFloat> = mutableMapOf()
): WriteFleet by writer, ReadFleet by reader, ContextualizedWriter<SpaceFloat>, Context<SpaceFloat> {

    companion object Factories {
        fun from(delegate: FleetMemoryRepository) = FleetContextualizedRepository(delegate, delegate)
    }

    override fun save(token: TargetToken, item: SpaceFloat): Either<SpacePangeeError, SpaceFloat> =
        writer.save(item).map {
            fleet[token] = it
            it
        }

    override fun byId(id: SpaceFloatIdentifier): Either<SpacePangeeError, SpaceFloat> =
        reader.byId(id)

    override fun get(token: TargetToken): SpaceFloat =
        if (token == none) {
            SpaceFloat.empty()
        } else {
            fleet[token]!!
        }

    override fun all(): List<SpaceFloat> = fleet.values.toList()

}