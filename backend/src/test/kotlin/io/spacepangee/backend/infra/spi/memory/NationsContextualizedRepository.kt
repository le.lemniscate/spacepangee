package io.spacepangee.backend.infra.spi.memory

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.models.Nation
import io.spacepangee.backend.domain.models.NationIdentifier
import io.spacepangee.backend.domain.repositories.ReadNations
import io.spacepangee.backend.domain.repositories.WriteNations
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.vavr.control.Either

class NationsContextualizedRepository(
    private val writer: WriteNations,
    private val reader: ReadNations,
    private val nations: MutableMap<TargetToken, Nation> = mutableMapOf()
): WriteNations, ReadNations, ContextualizedWriter<Nation>, Context<Nation> {

    companion object Factories {
        fun from(delegate: NationsMemoryRepository) = NationsContextualizedRepository(delegate, delegate)
    }

    override fun byId(id: NationIdentifier): Either<SpacePangeeError, Nation> {
        return reader.byId(id)
    }

    override fun save(nation: Nation): Either<SpacePangeeError, Nation> {
        return save(default, nation)
    }

    override fun save(token: TargetToken, item: Nation): Either<SpacePangeeError, Nation> =
        writer.save(item).map {
            nations[token] = it
            it
        }

    override fun get(token: TargetToken): Nation = nations[token]!!
    override fun all(): List<Nation> = nations.values.toList()

}