package io.spacepangee.backend.infra.spi.memory

import io.spacepangee.backend.domain.errors.SpacePangeeError
import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.vavr.control.Either

val default = TargetToken("default")
val foreign = TargetToken("foreign")
val origin = TargetToken("origin")
val target = TargetToken("target")
val none = TargetToken("none")
val alpha = TargetToken("alpha")
val beta = TargetToken("beta")
val other = TargetToken("other")

val X = TargetToken("X")
val Y = TargetToken("Y")

interface ContextualizedWriter<T> {
    fun save(token: TargetToken, item: T): Either<SpacePangeeError, T>
}

interface Context<T> {
    fun get(token: TargetToken): T
    fun all(): List<T>
}
