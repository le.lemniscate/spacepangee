package io.spacepangee.backend.infra.spi.memory

import io.spacepangee.backend.domain.usecases.dsl.TargetToken
import io.spacepangee.backend.domain.usecases.dsl.wrappers.IntWrapper
import kotlin.random.Random

class ParametersStore<T> {

    private val content = mutableMapOf<TargetToken, T>()

    operator fun get(key: TargetToken): T = content[key]!!
    operator fun set(key: TargetToken, value: T) {
        content[key] = value
    }
}

fun ParametersStore<IntWrapper>.`more than 0 and less than`(target: TargetToken) = IntWrapper(Random.nextInt(1, this[target].value))
fun ParametersStore<IntWrapper>.`more than`(target: TargetToken) = IntWrapper(Random.nextInt(this[target].value, 20000))
